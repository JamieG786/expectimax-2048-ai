# Expectimax 2048 AI

This is a expectimax algorithm written for the game 2048 in python. It achieves an average score of roughly 70,000 but scores are possible to go as high as 140,000.
The only heuristic this algorithm uses is to encourage the algorithm to follow the snake strategy.
This code was originally written for an assignment for the class CS310 at the University of Strathclyde.
To run this code all you need to do is pull all 3 python files and run the MainWithExpectimax.py file either in an IDE or from the command line.


An example of the game running:

![](2048Screenshot.png)