"""
Python 2048 Game : Basic Console Based User Interface For Game Play

"""
from py2048_classes import Board, Tile

import time
import math
import random

def expectimax(board, depth, player, move):
    if depth == 0:
        return utility(board, move)
    if player == 'MAX':
        score = 0
        for i in board.possible_moves():
            x = board.export_state()
            temp = Board(x)
            temp.make_move(i)
            
            x = expectimax(temp, depth-1, 'EXP', i)
            if x[0] >= score:
                score = x[0]
                move = x[1]
            
        return score, move
    
    if player == 'EXP':
        total = 0
        emptyCells = board.empty()
        for i in emptyCells:
            x = board.export_state()
            temp = Board(x)
            fourtile = Tile(2)
            temp.grid[i[1]][i[0]] = fourtile
            x = expectimax(temp, depth-1, 'MAX', move)
            total += (0.2 * x[0])
            
            x = board.export_state()
            temp = Board(x)
            twotile = Tile(1)
            temp.grid[i[1]][i[0]] = twotile
            x = expectimax(temp, depth-1, 'MAX', move)
            total += (0.8 * x[0])
        average = total/len(emptyCells)
        return average, move
            

def utility(board, move):

    score = 0     
    boardGrid = board.export_state()
    grid1Score = 0
    grid2Score = 0
    grid3Score = 0
    grid4Score = 0
    grid5Score = 0
    grid6Score = 0
    grid7Score = 0
    grid8Score = 0

    
    grid1 = [
        [2**15, 2**14, 2**13, 2**12],
        [2**8,  2**9,  2**10, 2**11],
        [2**7,  2**6,  2**5,  2**4],
        [2**0,  2**1,  2**2,  2**3]
        ]
    
    grid2 = [
        [2**12, 2**13, 2**14, 2**15],
        [2**11, 2**10, 2**9, 2**8],
        [2**1, 2**2, 2**3, 2**4],
        [2**0, 2**1, 2**2, 2**3]
        ]
    
    grid3 = [
        [2**3, 2**2, 2**1, 2**0],
        [2**4, 2**5, 2**6, 2**7],
        [2**11, 2**10, 2**9, 2**8],
        [2**12, 2**13, 2**14, 2**15]
        ]
    
    grid4 = [
        [2**0,  2**1, 2**2,  2**3],
        [2**7,  2**6, 2**5,  2**4],
        [2**8,  2**9,  2**10, 2**11],
        [2**15, 2**14, 2**13, 2**12]
        ]
    
    grid5 = [
        [2**15,  2**8, 2**7, 2**0],
        [2**14,  2**9, 2**6, 2**1],
        [2**13, 2**10, 2**5, 2**2],
        [2**12, 2**11, 2**4, 2**3]
        ]
    
    grid6 = [
        [2**12, 2**11, 2**4, 2**3],
        [2**13, 2**10, 2**5, 2**2],
        [2**14, 2**9,  2**6, 2**1],
        [2**15, 2**8,  2**7, 2**0]
        ]
    
    grid7 = [
        [2**3, 2**4, 2**11, 2**12],
        [2**2, 2**5, 2**10, 2**13],
        [2**1, 2**6, 2**9,  2**14],
        [2**0, 2**7, 2**8,  2**15]
        ]
        
    grid8 = [
        [2**0, 2**7, 2**8, 2**15],
        [2**1, 2**6, 2**9, 2**14],
        [2**2, 2**5, 2**10, 2**13],
        [2**3, 2**4, 2**11, 2**12]
        ]

    
    for x in range(0,4):
        for y in range(0,4):
            if boardGrid[x][y] != None:
                grid1Score += (board.grid[x][y].get_tile_value()) * grid1[x][y]
                grid2Score += (board.grid[x][y].get_tile_value()) * grid2[x][y]
                grid3Score += (board.grid[x][y].get_tile_value()) * grid3[x][y]
                grid4Score += (board.grid[x][y].get_tile_value()) * grid4[x][y]
                grid5Score += (board.grid[x][y].get_tile_value()) * grid5[x][y]
                grid6Score += (board.grid[x][y].get_tile_value()) * grid6[x][y]
                grid7Score += (board.grid[x][y].get_tile_value()) * grid7[x][y]
                grid8Score += (board.grid[x][y].get_tile_value()) * grid8[x][y]

        
    score = max(grid1Score, grid2Score, grid3Score, grid4Score, grid5Score, grid6Score, grid7Score, grid8Score)
 
    return score/(2**13), move

    
    
def main():
#    allmoves = ['UP','LEFT','DOWN','RIGHT']
    board = Board()
    board.add_random_tiles(2)
    print("main code")

    move_counter = 0
    move = None
    move_result = False
    
    
    overalltime=time.time()
    while True:
        print("Number of successful moves:{}, Last move attempted:{}:, Move status:{}".format(move_counter, move, move_result))
        print(board)
       # print(board.print_metrics())
        if board.possible_moves()==[]:
            if (board.get_max_tile()[0]<2048):
                print("You lost!")
            else:
                print("Congratulations - you won!")
            break
        begin = time.time()
        
        topScore = 0
        for i in board.possible_moves():
            x = board.export_state()
            temp = Board(x)
            temp.make_move(i)

            x = expectimax(temp, 4, 'EXP', i)
            
            if x[0] >= topScore:
                topScore = x[0]
                move = x[1]
        
        board.make_move(move)
        
        
######################################  Do not modify 4 lines below      
######################################
        print("Move time: ", time.time()-begin)
        board.add_random_tiles(1)
        move_counter = move_counter + 1
    print("Average time per move:", (time.time()-overalltime)/move_counter)
    print("Total time:", (time.time()-overalltime)/60, " mins")
    

if __name__ == "__main__":
    main()
    

  

